package de.rwth.swc.sqa.service;
import de.rwth.swc.sqa.exception.InvalidTicketException;

import de.rwth.swc.sqa.model.Ticket;
import de.rwth.swc.sqa.model.TicketRequest;
import de.rwth.swc.sqa.model.TicketValidationRequest;
import de.rwth.swc.sqa.repository.TicketRepository;
import de.rwth.swc.sqa.util.TicketUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Objects;
import java.time.format.DateTimeFormatter;  
import java.time.LocalDateTime;
import java.time.LocalDate;
import java.time.format.ResolverStyle;

@Service
public class TicketService {
    private final TicketRepository ticketRepository;
    private final DiscountCardService discountCardService;

    @Autowired
    public TicketService(TicketRepository ticketRepository, DiscountCardService discountCardService) {
        this.ticketRepository = ticketRepository;
        this.discountCardService = discountCardService;
    }

   public Ticket buyTicket(TicketRequest ticketRequest) throws InvalidTicketException {
        String birthdate = ticketRequest.getBirthdate();
        String validFrom = ticketRequest.getValidFrom();
        TicketRequest.ValidForEnum validForRequest = ticketRequest.getValidFor();
        Ticket.ValidForEnum validFor = Ticket.ValidForEnum.fromValue(validForRequest.getValue());
        TicketRequest.ZoneEnum zoneRequest = ticketRequest.getZone();

        if(zoneRequest == null || !TicketUtils.validateBirthdate(birthdate) || !TicketUtils.validateValidFrom(validFrom)) {
            throw new IllegalStateException();
        }

        Ticket.ZoneEnum zone = Ticket.ZoneEnum.fromValue(zoneRequest.toString());

        boolean disabled = ticketRequest.getDisabled() != null ? ticketRequest.getDisabled() : false;
        boolean student = ticketRequest.getStudent() != null ? ticketRequest.getStudent() : false;
        Integer discountCard = ticketRequest.getDiscountCard() != null ? ticketRequest.getDiscountCard() : 0;
         if (student) {
            if (validFor.getValue().equals("1h") ||validFor.getValue().equals("24h")) {
                throw new InvalidTicketException();
            }
            DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE.withResolverStyle(ResolverStyle.STRICT);
            LocalDate birthdateLocalDate = LocalDate.parse(birthdate, formatter);
            if (birthdateLocalDate.plusYears(28).isBefore(LocalDate.now())) {
                throw new InvalidTicketException();
            };
        }

        Ticket ticket = new Ticket();
        ticket.setBirthdate(birthdate);
        ticket.setValidFrom(validFrom);
        ticket.setDisabled(disabled);
        ticket.setStudent(student);
        ticket.setDiscountCard(discountCard);
        ticket.setValidFor(validFor);
        ticket.setZone(zone);

        return ticketRepository.saveTicket(ticket);
    }
    
   public boolean validateTicket(TicketValidationRequest ticketRequest) {
      
	   if (Objects.isNull(ticketRepository.getTicket(ticketRequest.getTicketId())))
       {
           return false;
       }else {
           try {
               Ticket ticket = ticketRepository.getTicket(ticketRequest.getTicketId());
               // Datum in den korrespondierenden Grenzen für Tag,Stunde, Jahr
               if ((ticket.getDiscountCard() == 25 || ticket.getDiscountCard() == 50) && !(discountCardService.getAllDiscountCardIds().contains(ticketRequest.getDiscountCardId()))){
                   return false;
               }
               if (ticket.getValidFor().getValue().equals(Ticket.ValidForEnum._1D.getValue()) && ticketRequest.getZone().getValue().equals(ticket.getZone().getValue()) && ticketRequest.getDisabled() == ticket.getDisabled() && ticketRequest.getStudent() == (ticket.getStudent())) {
                   DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
                   LocalDateTime dateMin = LocalDateTime.parse(ticket.getValidFrom(), formatter);
                   LocalDateTime dateMax = dateMin.plusDays(1);
                   LocalDateTime timeRequest = LocalDateTime.parse(ticketRequest.getDate(), formatter);
                   return ((timeRequest.isAfter(dateMin) && timeRequest.isBefore(dateMax)) || timeRequest.isEqual(dateMin) || timeRequest.isEqual(dateMax));

               } else if (ticket.getValidFor().getValue().equals(Ticket.ValidForEnum._30D.getValue()) && ticketRequest.getZone().getValue().equals(ticket.getZone().getValue()) && ticketRequest.getDisabled() == ticket.getDisabled() && ticketRequest.getStudent() == (ticket.getStudent())) {
                   DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
                   LocalDateTime dateMin = LocalDateTime.parse(ticket.getValidFrom(), formatter);
                   LocalDateTime dateMax = dateMin.plusDays(30);
                   LocalDateTime timeRequest = LocalDateTime.parse(ticketRequest.getDate(), formatter);
                   return ((timeRequest.isAfter(dateMin) && timeRequest.isBefore(dateMax)) || timeRequest.isEqual(dateMin) || timeRequest.isEqual(dateMax));

               } else if (ticket.getValidFor().getValue().equals(Ticket.ValidForEnum._1Y.getValue()) && ticketRequest.getZone().getValue().equals(ticket.getZone().getValue()) && ticketRequest.getDisabled() == ticket.getDisabled() && ticketRequest.getStudent() == (ticket.getStudent())) {
                   DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
                   LocalDateTime dateMin = LocalDateTime.parse(ticket.getValidFrom(), formatter);
                   LocalDateTime dateMax = dateMin.plusYears(1);
                   LocalDateTime timeRequest = LocalDateTime.parse(ticketRequest.getDate(), formatter);
                   return ((timeRequest.isAfter(dateMin) && timeRequest.isBefore(dateMax)) || timeRequest.isEqual(dateMin) || timeRequest.isEqual(dateMax));

               } else if (ticket.getValidFor().getValue().equals(Ticket.ValidForEnum._1H.getValue()) && ticketRequest.getZone().getValue().equals(ticket.getZone().getValue()) && ticketRequest.getDisabled() == ticket.getDisabled() && ticketRequest.getStudent() == (ticket.getStudent())) {
                   DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
                   LocalDateTime dateMin = LocalDateTime.parse(ticket.getValidFrom(), formatter);
                   LocalDateTime dateMax = dateMin.plusHours(1);
                   LocalDateTime timeRequest = LocalDateTime.parse(ticketRequest.getDate(), formatter);
                   return ((timeRequest.isAfter(dateMin) && timeRequest.isBefore(dateMax)) || timeRequest.isEqual(dateMin) || timeRequest.isEqual(dateMax));
               } else {
                   return false;
               }
           } catch (Exception e) {
               return false;
           }
       }
    }
}

