package de.rwth.swc.sqa.service;

import de.rwth.swc.sqa.model.Customer;
import de.rwth.swc.sqa.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerService {
    private final CustomerRepository customerRepository;

    @Autowired
    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public Customer addCustomer(Long id, Customer customer) {
        return customerRepository.addCustomer(id, customer);
    }

    public boolean customerExists(Long customerId) {
        return customerRepository.containsCustomer(customerId);
    }
}
