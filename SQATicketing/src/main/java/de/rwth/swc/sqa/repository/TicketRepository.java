package de.rwth.swc.sqa.repository;

import de.rwth.swc.sqa.model.Ticket;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Repository
public class TicketRepository {
    private final Map<Long, Ticket> TICKET_DB;

    public TicketRepository() {
        TICKET_DB = new HashMap<>();
    }

    public Ticket saveTicket(Ticket ticket) {
        Long id = UUID.randomUUID().getMostSignificantBits() & Long.MAX_VALUE;
        ticket.setId(id);
        TICKET_DB.put(id, ticket);
        return ticket;
    }

    public Ticket getTicket(Long id) {
        return TICKET_DB.get(id);
    }
}
