package de.rwth.swc.sqa.repository;

import de.rwth.swc.sqa.model.Customer;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class CustomerRepository {
    private final Map<Long, Customer> CUSTOMER_DB;

    public CustomerRepository() {
        CUSTOMER_DB = new HashMap<>();
    }

    public Customer addCustomer(Long key, Customer customer){
        CUSTOMER_DB.put(key, customer);
        return customer;
    }

    public boolean containsCustomer(Long customerId) {
        return CUSTOMER_DB.containsKey(customerId);
    }

}
