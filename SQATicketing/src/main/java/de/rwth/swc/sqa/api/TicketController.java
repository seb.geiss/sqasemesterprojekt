package de.rwth.swc.sqa.api;

import de.rwth.swc.sqa.model.Ticket;
import de.rwth.swc.sqa.model.TicketRequest;
import de.rwth.swc.sqa.model.TicketValidationRequest;
import de.rwth.swc.sqa.service.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.request.NativeWebRequest;

import java.util.Optional;

@Controller
public class TicketController implements TicketsApi{
    private final TicketService ticketService;

    @Autowired
    public TicketController(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    @Override
    public Optional<NativeWebRequest> getRequest() {
        return TicketsApi.super.getRequest();
    }

    @Override
    public ResponseEntity<Ticket> buyTicket(TicketRequest body) {
        Ticket ticket;
        try {
            ticket = ticketService.buyTicket(body);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(ticket, HttpStatus.CREATED);
    }

   @Override
    public ResponseEntity<Void> validateTicket(TicketValidationRequest body) {
        boolean valid = ticketService.validateTicket(body);

    if (valid){
    return new ResponseEntity<>(HttpStatus.OK);
    }
    else {
    return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }
    }
}
