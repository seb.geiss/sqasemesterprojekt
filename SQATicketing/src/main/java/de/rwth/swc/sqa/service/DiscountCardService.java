package de.rwth.swc.sqa.service;

import de.rwth.swc.sqa.exception.CustomerNotFoundException;
import de.rwth.swc.sqa.exception.DiscountCardIntersectionException;
import de.rwth.swc.sqa.exception.DiscountCardNotFoundException;
import de.rwth.swc.sqa.exception.InvalidRequestBodyException;
import de.rwth.swc.sqa.model.DiscountCard;
import de.rwth.swc.sqa.repository.DiscountCardRepository;
import de.rwth.swc.sqa.util.DiscountCardUtils;
import de.rwth.swc.sqa.util.TicketUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.ResolverStyle;
import java.util.List;
import java.util.Set;

@Service
public class DiscountCardService {
    private final DiscountCardRepository discountCardRepository;
    private final CustomerService customerService;

    @Autowired
    public DiscountCardService(DiscountCardRepository discountCardRepository, CustomerService customerService) {
        this.discountCardRepository = discountCardRepository;
        this.customerService = customerService;
    }

    public Set<Long> getAllDiscountCardIds() {
        return discountCardRepository.getAllDiscountCardIds();
    }

    public DiscountCard saveDiscountCard(Long customerId, DiscountCard discountCard) throws CustomerNotFoundException, InvalidRequestBodyException, DiscountCardIntersectionException {
        if (!DiscountCardUtils.validateValidFrom(discountCard.getValidFrom())
                || !DiscountCardUtils.validateType(discountCard.getType())) {
            throw new InvalidRequestBodyException();
        }
        if (!customerService.customerExists(customerId)) {
            throw new CustomerNotFoundException();
        }

        List<DiscountCard> discountCards = discountCardRepository.getAllDiscountCards(customerId);
        if (discountCards.size() == 0) {
            return discountCardRepository.saveDiscountCard(customerId, discountCard);
        }

        for (DiscountCard current : discountCards) {
            if (haveIntersection(discountCard, current)) {
                throw new DiscountCardIntersectionException();
            }
        }

        return discountCardRepository.saveDiscountCard(customerId, discountCard);
    }

    public List<DiscountCard> getAllDiscountCards(Long customerId) throws CustomerNotFoundException, DiscountCardNotFoundException {
        if (!customerService.customerExists(customerId)) {
            throw new CustomerNotFoundException();
        }
        List<DiscountCard> discountCards = discountCardRepository.getAllDiscountCards(customerId);
        if (discountCards.size() == 0) {
            throw new DiscountCardNotFoundException();
        }
        return discountCards;
    }

    private boolean haveIntersection(final DiscountCard toInsertDiscountCard, final DiscountCard existingDiscountCard) {
        String validFromInsertion = toInsertDiscountCard.getValidFrom();
        String validForInsertion = toInsertDiscountCard.getValidFor().getValue();
        String validFromExisting = existingDiscountCard.getValidFrom();
        String validForExisting = existingDiscountCard.getValidFor().getValue();

        DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE.withResolverStyle(ResolverStyle.STRICT);
        LocalDate startDateInsertion = LocalDate.parse(validFromInsertion, formatter);
        LocalDate endDateInsertion;
        if (validForInsertion.equals("30d")) {
            endDateInsertion = startDateInsertion.plusDays(30);
        } else {
            endDateInsertion = startDateInsertion.plusYears(1);
        }

        LocalDate startDateExisting = LocalDate.parse(validFromExisting, formatter);
        LocalDate endDateExisting;
        if (validForExisting.equals("30d")) {
            endDateExisting = startDateExisting.plusDays(30);
        } else {
            endDateExisting = startDateExisting.plusYears(1);
        }

        if(startDateInsertion.isBefore(startDateExisting)) {
            return startDateExisting.isBefore(endDateInsertion);
        } else {
            return startDateInsertion.isBefore(endDateExisting);
        }
    }
}
