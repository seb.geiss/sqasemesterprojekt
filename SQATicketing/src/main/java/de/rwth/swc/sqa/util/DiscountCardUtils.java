package de.rwth.swc.sqa.util;

import org.apache.commons.lang3.StringUtils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.ResolverStyle;

public class DiscountCardUtils {
    public static boolean validateType(Integer type) {
        return ((type != null) && (type == 25 || type == 50));
    }

    public static boolean validateValidFrom(String validFrom) {
        if (StringUtils.isBlank(validFrom)) return false;
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE.withResolverStyle(ResolverStyle.STRICT);
            LocalDate date = LocalDate.parse(validFrom, formatter);
            return date.isAfter(LocalDate.parse("1899-12-31", formatter));
        } catch (Exception e) {
            return false;
        }
    }
}
