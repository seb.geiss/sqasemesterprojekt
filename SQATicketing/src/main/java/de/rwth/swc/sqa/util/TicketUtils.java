package de.rwth.swc.sqa.util;

import org.apache.commons.lang3.StringUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.ResolverStyle;
import java.time.temporal.ChronoUnit;

public class TicketUtils {
    public static boolean validateBirthdate(String birthdate) {
        if (StringUtils.isBlank(birthdate)) return false;
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE.withResolverStyle(ResolverStyle.STRICT);
            LocalDate date = LocalDate.parse(birthdate, formatter);
            LocalDate today = LocalDate.now();
            return date.isBefore(today) && date.isAfter(LocalDate.parse("1899-12-31", formatter));
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean validateValidFrom(String validFrom) {
        if (StringUtils.isBlank(validFrom) || validFrom.length() != 19) return false;

        try {
            DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME.withResolverStyle(ResolverStyle.STRICT);
            LocalDateTime date = LocalDateTime.parse(validFrom, formatter).truncatedTo(ChronoUnit.SECONDS);
            LocalDateTime now = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS);
            return date.isBefore(now);
        } catch (Exception e) {
            return false;
        }
    }
}
