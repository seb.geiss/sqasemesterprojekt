package de.rwth.swc.sqa.repository;

import de.rwth.swc.sqa.model.DiscountCard;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class DiscountCardRepository {
    private final Map<Long, DiscountCard> DISCOUNTCARD_DB;

    public DiscountCardRepository() {
        DISCOUNTCARD_DB = new HashMap<>();
    }

    public DiscountCard saveDiscountCard(Long customerId, DiscountCard discountCard) {
        Long id = UUID.randomUUID().getMostSignificantBits() & Long.MAX_VALUE;
        discountCard.setId(id);
        DISCOUNTCARD_DB.put(id, discountCard);
        return discountCard;
    }

    public Set<Long> getAllDiscountCardIds() {
        Set<Long> discountCardIds = new HashSet<>();
        for (DiscountCard discountCard : DISCOUNTCARD_DB.values()) {
            discountCardIds.add( discountCard.getId() );
        }
        return discountCardIds;
    }

    public List<DiscountCard> getAllDiscountCards(Long customerId) {
        List<DiscountCard> discountCards = new ArrayList<>();
        for (DiscountCard dc : DISCOUNTCARD_DB.values()) {
            if (dc.getCustomerId().equals(customerId)) {
                discountCards.add(dc);
            }
        }
        return discountCards;
    }
}
