package de.rwth.swc.sqa.api;

import de.rwth.swc.sqa.exception.CustomerNotFoundException;
import de.rwth.swc.sqa.exception.DiscountCardIntersectionException;
import de.rwth.swc.sqa.exception.DiscountCardNotFoundException;
import de.rwth.swc.sqa.exception.InvalidRequestBodyException;
import de.rwth.swc.sqa.model.Customer;
import de.rwth.swc.sqa.model.DiscountCard;
import de.rwth.swc.sqa.service.CustomerService;
import de.rwth.swc.sqa.service.DiscountCardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.request.NativeWebRequest;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Controller
public class CustomerController implements CustomersApi{
    private final CustomerService customerService;
    private final DiscountCardService discountCardService;

    @Autowired
    public CustomerController(CustomerService customerService, DiscountCardService discountCardService) {
        this.customerService = customerService;
        this.discountCardService = discountCardService;
    }

    @Override
    public Optional<NativeWebRequest> getRequest() {
        return CustomersApi.super.getRequest();
    }

    @Override

    public ResponseEntity<Customer> addCustomer(Customer body) {



        if(body.getBirthdate()== null) {
            return new ResponseEntity<Customer>(HttpStatus.BAD_REQUEST);
        }

        if(body.getDisabled()==null){
            body.setDisabled(false);
        }

        if(body.getId() == null||body.getId() != null)
        {
            body.setId(UUID.randomUUID().getMostSignificantBits() & Long.MAX_VALUE);
        }

        try {
            customerService.addCustomer(body.getId(), body);
        }
        catch (Exception e)
        {
            return new ResponseEntity<Customer>(HttpStatus.BAD_REQUEST);
        }


        return new ResponseEntity<Customer>(body, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<DiscountCard> addDiscountCardToCustomer(Long customerId, DiscountCard body) {
        if (!customerId.equals(body.getCustomerId())) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        try{
            DiscountCard discountCard = discountCardService.saveDiscountCard(customerId, body);
            return new ResponseEntity<>(discountCard, HttpStatus.CREATED);
        } catch (CustomerNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (InvalidRequestBodyException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (DiscountCardIntersectionException e) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }

    @Override
    public ResponseEntity<List<DiscountCard>> getCustomerDiscountCards(Long customerId) {
        try {
            List<DiscountCard> discountCards = discountCardService.getAllDiscountCards(customerId);
            return new ResponseEntity<>(discountCards, HttpStatus.OK);
        } catch (CustomerNotFoundException | DiscountCardNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
