package de.rwth.swc.sqa;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

import io.restassured.matcher.RestAssuredMatchers;
import org.hamcrest.Matchers.*;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import java.util.LinkedList;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
@TestInstance(Lifecycle.PER_CLASS)
public class GetDiscountCardTest {

    private static final String PATH = "/customers/1/discountcards";

    private List<String> customerList = new LinkedList<String>();

    @LocalServerPort
    private int port;
    @BeforeEach
    public void setUp() {
        RestAssured.port = port;
    }

    @BeforeAll
    public void createCustomerForValidation()
    {
        setUp();
        String customer = "{ \n" +
                "    \"birthdate\" : \"1992-01-01\",\n" +
                "    \"disabled\" : true, \n" +
                "    \"id\" : 21 \n" +
                "}";

        this.customerList.add(given().
                contentType(ContentType.JSON).
                body(customer)
                .post("/customers")
                .getBody()
                .jsonPath()
                .get("id").toString());


        customer = "{ \n" +
                "    \"birthdate\" : \"1992-01-01\",\n" +
                "    \"disabled\" : true, \n" +
                "    \"id\" : 22 \n" +
                "}";

        this.customerList.add(given().
                contentType(ContentType.JSON).
                body(customer)
                .post("/customers").getBody().jsonPath().get("id").toString());



        String discountCard = "{\n" +
                "    \"validFor\" : \"30d\", \n" +
                "    \"customerId\" : "+Long.valueOf(customerList.get(0))+",\n" +
                "    \"id\" : 1,\n" +
                "    \"validFrom\" : \"1994-01-01\",\n" +
                "    \"type\" : \"25\" \n" +
                "}";


        given().
                contentType(ContentType.JSON).
                body(discountCard)
                .post("/customers/"+ customerList.get(0)+"/discountcards").then().statusCode(201);




    }

    @Test
    public void getDiscountCard_whenValidDiscountCardExists_returnDiscountCard() {

        String customer = customerList.get(0);


        given().
                contentType(ContentType.JSON)
                .get("/customers/"+customer+"/discountcards")
                .then()
                .statusCode(200)
                .body(
                        "id", org.hamcrest.Matchers.is(org.hamcrest.Matchers.notNullValue()),
                        "customerId",  org.hamcrest.Matchers.contains(Long.parseLong(customer)),
                        "type", org.hamcrest.Matchers.contains(25),
                        "validFrom", org.hamcrest.Matchers.contains("1994-01-01"),
                        "validFor", org.hamcrest.Matchers.contains("30d")
                );

    }

    @Test
    public void getDiscountCard_whenInValidUserId_return400() {
        given().
                contentType(ContentType.JSON)
                .get("/customers/a/discountcards")
                .then()
                .statusCode(400)
                ;

    }

    @Test
    public void getDiscountCard_whenUserWithoutCard_return404() {
        String customer = customerList.get(1);
        given().
                contentType(ContentType.JSON)
                .get("/customers/"+customer+"/discountcards")
                .then()
                .statusCode(404);
    }

    @Test
    public void getDiscountCard_whenUserNotExisting_return404() {
        given().
                contentType(ContentType.JSON)
                .get("/customers/42069/discountcards")
                .then()
                .statusCode(404);
    }


}
