package de.rwth.swc.sqa;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;

import io.restassured.matcher.RestAssuredMatchers;
import org.hamcrest.Matchers.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import static io.restassured.RestAssured.given;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
@TestInstance(Lifecycle.PER_CLASS)
public class TicketTest {


    private static final String PATH = "/customers/1/discountcards";

    @LocalServerPort
    private int port;
    @BeforeEach
    public void setUp() {
        RestAssured.port = port;
    }

    @BeforeAll
    public void createCustomerForValidation()
    {
        setUp();

        String customer = "{\"birthdate\" : \"1992-01-01\"" +
                ",\"disabled\" : true," +
                " \"id\" : 31}";

        given().
                contentType(ContentType.JSON).
                body(customer)
                .post("/customers");


        String discountCard =  "{ \"validFor\" : \"30d\"," +
                " \"customerId\" : 31, " +
                "\"id\" : 0, " +
                "\"validFrom\" : \"1992-01-01\", " +
                "\"type\" : 1 }";

        given().
                contentType(ContentType.JSON).
                body(discountCard)
                .post("/customers/31/discountcards");

    }


    @Test
    public void ticket_givenValidTicket_returnSameTicket() {
        String ticket = "{\n" +
                "    \"birthdate\" : \"1992-01-01\",\n" +
                "    \"validFor\" : \"1h\", \n" +
                "    \"zone\" : \"A\", \n" +
                "    \"student\" : false, \n" +
                "    \"discountCard\" : 25,\n" +
                "    \"disabled\" : false, \n" +
                "    \"id\" : 1, \n" +
                "    \"validFrom\" : \"2020-10-29T10:38:59\"\n" +
                "}";

        given().
                contentType(ContentType.JSON).
                body(ticket)
                .post("/tickets").then()
                .statusCode(201)
                .body(
                "id",   org.hamcrest.Matchers.is(org.hamcrest.Matchers.notNullValue()),
                "validFrom", org.hamcrest.Matchers.is("2020-10-29T10:38:59"),
                "birthdate", org.hamcrest.Matchers.is("1992-01-01"),
                "validFor", org.hamcrest.Matchers.is("1h"),
                "disabled", org.hamcrest.Matchers.is(false),
                "discountCard", org.hamcrest.Matchers.is(25),
                "zone", org.hamcrest.Matchers.is("A"),
                "student", org.hamcrest.Matchers.is(false)

                );

    }



  //  "buy a new ticket. disabled, discountCard and
    //  student are optional and should default to false if
    //  not set. See Ticket table for avaiable Tickettypes"

    @Test
    public void ticket_givenValidTicketWhereOptionalsNotGiven_returnSameTicketWithOptionalsFalse() {
        String ticket = "{\n" +
                "    \"birthdate\" : \"1992-01-01\",\n" +
                "    \"validFor\" : \"1h\", \n" +
                "    \"zone\" : \"A\", \n" +
                "    \"validFrom\" : \"2020-10-29T10:38:59\"\n" +
                "}";

        given().
                contentType(ContentType.JSON).
                body(ticket)
                .post("/tickets").then()
                .statusCode(201)
                .body(

                        "id",   org.hamcrest.Matchers.is(org.hamcrest.Matchers.notNullValue()),
                        "validFrom", org.hamcrest.Matchers.is("2020-10-29T10:38:59"),
                        "birthdate", org.hamcrest.Matchers.is("1992-01-01"),
                        "validFor", org.hamcrest.Matchers.is("1h"),
                        "disabled", org.hamcrest.Matchers.is(false),
                        "discountCard", org.hamcrest.Matchers.is(0),
                        "zone", org.hamcrest.Matchers.is("A"),
                        "student", org.hamcrest.Matchers.is(false)

                );

    }


    @ParameterizedTest
    @ValueSource(strings = {
            "{\n" +
                    "    \"birthdate\" : \"Mulm\",\n" +
                    "    \"validFor\" : \"1h\", \n" +
                    "    \"zone\" : \"A\", \n" +
                    "    \"student\" : true, \n" +
                    "    \"discountCard\" : 25,\n" +
                    "    \"disabled\" : true, \n" +
                    "    \"id\" : 1, \n" +
                    "    \"validFrom\" : \"2020-10-29T10:38:59\"\n" +
                    "}",
            "{\n" +
                    "    \"birthdate\" : \"1992-01-01\",\n" +
                    "    \"validFor\" : \"9h\", \n" +
                    "    \"zone\" : \"A\", \n" +
                    "    \"student\" : true, \n" +
                    "    \"discountCard\" : 25,\n" +
                    "    \"disabled\" : true, \n" +
                    "    \"id\" : 1, \n" +
                    "    \"validFrom\" : \"2020-10-29T10:38:59\"\n" +
                    "}",
            "{\n" +
                    "    \"birthdate\" : \"1992-01-01\",\n" +
                    "    \"validFor\" : \"1h\", \n" +
                    "    \"zone\" : \"D\", \n" +
                    "    \"student\" : true, \n" +
                    "    \"discountCard\" : 25,\n" +
                    "    \"disabled\" : true, \n" +
                    "    \"id\" : 1, \n" +
                    "    \"validFrom\" : \"2020-10-29T10:38:59\"\n" +
                    "}",
            "{\n" +
                    "    \"birthdate\" : \"1992-01-01\",\n" +
                    "    \"validFor\" : \"1h\", \n" +
                    "    \"zone\" : \"A\", \n" +
                    "    \"student\" : true, \n" +
                    "    \"discountCard\" : 25,\n" +
                    "    \"disabled\" : true, \n" +
                    "    \"id\" : 1, \n" +
                    "    \"validFrom\" : \"Datum Leude\"\n" +
                    "}"
    })
    public void ticket_givenSyntacticalInvalidTicket_return(String ticket) {
        given().
                contentType(ContentType.JSON).
                body(ticket)
                .post("/tickets").then()
                .statusCode(400);


    }

    @ParameterizedTest
    @ValueSource(strings = {
            "{\n" +
                    "    \"birthdate\" : \"2002-01-01\",\n" +
                    "    \"validFor\" : \"1h\", \n" +
                    "    \"zone\" : \"A\", \n" +
                    "    \"student\" : true, \n" +
                    "    \"discountCard\" : 25,\n" +
                    "    \"disabled\" : true, \n" +
                    "    \"id\" : 1, \n" +
                    "    \"validFrom\" : \"2020-10-29T10:38:59\"\n" +
                    "}",
            "{\n" +
                    "    \"birthdate\" : \"2002-01-01\",\n" +
                    "    \"validFor\" : \"1h\", \n" +
                    "    \"zone\" : \"B\", \n" +
                    "    \"student\" : true, \n" +
                    "    \"discountCard\" : 25,\n" +
                    "    \"disabled\" : true, \n" +
                    "    \"id\" : 1, \n" +
                    "    \"validFrom\" : \"2020-10-29T10:38:59\"\n" +
                    "}",
            "{\n" +
                    "    \"birthdate\" : \"2002-01-01\",\n" +
                    "    \"validFor\" : \"1h\", \n" +
                    "    \"zone\" : \"C\", \n" +
                    "    \"student\" : true, \n" +
                    "    \"discountCard\" : 25,\n" +
                    "    \"disabled\" : true, \n" +
                    "    \"id\" : 1, \n" +
                    "    \"validFrom\" : \"2020-10-29T10:38:59\"\n" +
                    "}",
            "{\n" +
                    "    \"birthdate\" : \"1992-01-01\",\n" +
                    "    \"validFor\" : \"30d\", \n" +
                    "    \"zone\" : \"A\", \n" +
                    "    \"student\" : true, \n" +
                    "    \"discountCard\" : 25,\n" +
                    "    \"disabled\" : true, \n" +
                    "    \"id\" : 1, \n" +
                    "    \"validFrom\" : \"2020-10-29T10:38:59\"\n" +
                    "}",
            "{\n" +
                    "    \"birthdate\" : \"1992-01-01\",\n" +
                    "    \"validFor\" : \"30d\", \n" +
                    "    \"zone\" : \"B\", \n" +
                    "    \"student\" : true, \n" +
                    "    \"discountCard\" : 25,\n" +
                    "    \"disabled\" : true, \n" +
                    "    \"id\" : 1, \n" +
                    "    \"validFrom\" : \"2020-10-29T10:38:59\"\n" +
                    "}",
            "{\n" +
                    "    \"birthdate\" : \"1992-01-01\",\n" +
                    "    \"validFor\" : \"30d\", \n" +
                    "    \"zone\" : \"C\", \n" +
                    "    \"student\" : true, \n" +
                    "    \"discountCard\" : 25,\n" +
                    "    \"disabled\" : true, \n" +
                    "    \"id\" : 1, \n" +
                    "    \"validFrom\" : \"2020-10-29T10:38:59\"\n" +
                    "}",
            "{\n" +
                    "    \"birthdate\" : \"1992-01-01\",\n" +
                    "    \"validFor\" : \"1y\", \n" +
                    "    \"zone\" : \"A\", \n" +
                    "    \"student\" : true, \n" +
                    "    \"discountCard\" : 25,\n" +
                    "    \"disabled\" : true, \n" +
                    "    \"id\" : 1, \n" +
                    "    \"validFrom\" : \"2020-10-29T10:38:59\"\n" +
                    "}",
            "{\n" +
                    "    \"birthdate\" : \"1992-01-01\",\n" +
                    "    \"validFor\" : \"1y\", \n" +
                    "    \"zone\" : \"B\", \n" +
                    "    \"student\" : true, \n" +
                    "    \"discountCard\" : 25,\n" +
                    "    \"disabled\" : true, \n" +
                    "    \"id\" : 1, \n" +
                    "    \"validFrom\" : \"2020-10-29T10:38:59\"\n" +
                    "}",
            "{\n" +
                    "    \"birthdate\" : \"1992-01-01\",\n" +
                    "    \"validFor\" : \"1y\", \n" +
                    "    \"zone\" : \"C\", \n" +
                    "    \"student\" : true, \n" +
                    "    \"discountCard\" : 25,\n" +
                    "    \"disabled\" : true, \n" +
                    "    \"id\" : 1, \n" +
                    "    \"validFrom\" : \"2020-10-29T10:38:59\"\n" +
                    "}"
    })
    public void ticket_givenSemanticalInvalidTicket_return(String ticket) {
        given().
                contentType(ContentType.JSON).
                body(ticket)
                .post("/tickets").then()
                .statusCode(400);


    }

}
