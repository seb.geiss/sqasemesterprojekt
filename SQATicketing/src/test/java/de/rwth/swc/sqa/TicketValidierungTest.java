package de.rwth.swc.sqa;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;

import io.restassured.matcher.RestAssuredMatchers;
import org.hamcrest.Matchers.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.DependsOn;

import java.util.LinkedList;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
@TestInstance(Lifecycle.PER_CLASS)
public class TicketValidierungTest {

    @LocalServerPort
    private int port;
    @BeforeEach
    public void setUp() {
        RestAssured.port = port;
    }

    private List<String> ticketList = new LinkedList<String>();

    private List<String> customerList = new LinkedList<String>();

    String validDiscountCardID;

    @BeforeAll
    public void createTicketForValidation()
    {
        setUp();
        create_valid_discountCard();
        validDiscountCardID = given().
                contentType(ContentType.JSON)
                .get("/customers/"+customerList.get(0)+"/discountcards")
                .getBody().jsonPath().get("id").toString();

        validDiscountCardID = validDiscountCardID.substring(1, validDiscountCardID.length() - 1);


        String ticket = "{\n" +
                "    \"birthdate\" : \"2002-01-01\",\n" +
                "    \"validFor\" : \"1y\", \n" +
                "    \"zone\" : \"A\", \n" +
                "    \"student\" : true, \n" +
                "    \"discountCard\" : 25,\n"+
                "    \"disabled\" : true, \n" +
                "    \"id\" : 1, \n" +
                "    \"validFrom\" : \"2020-10-29T10:38:59\"\n" +
                "}";



        ticketList.add(given().
                contentType(ContentType.JSON).
                body(ticket)
                .post("/tickets").getBody().jsonPath().get("id").toString());



        ticket = "{\n" +
                "    \"birthdate\" : \"2002-01-01\",\n" +
                "    \"validFor\" : \"1y\", \n" +
                "    \"zone\" : \"A\", \n" +
                "    \"id\" : 1, \n" +
                "    \"validFrom\" : \"2020-10-29T10:38:59\"\n" +
                "}";



        ticketList.add(given().
                contentType(ContentType.JSON).
                body(ticket)
                .post("/tickets").getBody().jsonPath().get("id").toString());

    }

    private void create_valid_discountCard(){

        String customer = "{ \n" +
                "    \"birthdate\" : \"1992-01-01\",\n" +
                "    \"disabled\" : true, \n" +
                "    \"id\" : 22 \n" +
                "}";

        this.customerList.add(given().
                contentType(ContentType.JSON).
                body(customer)
                .post("/customers")
                .getBody()
                .jsonPath()
                .get("id").toString());



        String discountCard = "{\n" +
                "    \"validFor\" : \"30d\", \n" +
                "    \"customerId\" : "+Long.valueOf(customerList.get(0))+",\n" +
                "    \"id\" : 1,\n" +
                "    \"validFrom\" : \"1994-01-01\",\n" +
                "    \"type\" : \"25\" \n" +
                "}";


        given().
                contentType(ContentType.JSON).
                body(discountCard)
                .post("/customers/"+ customerList.get(0)+"/discountcards").then().statusCode(201);



    }


    @Test
    public void validateTicket_giveValidTicket_Return200(){
        String ticket = this.ticketList.get(1);
        String validation = "{\n" +
                "    \"ticketId\": "+ticket+" ,\n" +
                "    \"zone\": \"A\" ,\n" +
                "    \"date\": \"2020-10-29T10:38:59\" ,\n" +
                "    \"disabled\": false ,\n" +
                "    \"student\": false\n" +
                "}";
        given().
                contentType(ContentType.JSON).
                body(validation)
                .post("/tickets/validate").then().statusCode(200);

    }

    @Test
    public void validateTicket_giveValidTicketWithValidDiscountCard_Return200(){
        String ticket = this.ticketList.get(0);
        String validation = "{\n" +
                "    \"ticketId\": "+ticket+" ,\n" +
                "    \"zone\": \"A\" ,\n" +
                "    \"date\": \"2020-10-29T10:38:59\" ,\n" +
                "    \"discountCardId\": "+validDiscountCardID+" ,\n" +
                "    \"disabled\": true ,\n" +
                "    \"student\": true\n" +
                "}";
        given().
                contentType(ContentType.JSON).
                body(validation)
                .post("/tickets/validate").then().statusCode(200);

    }

    @Test
    public void validateTicket_giveValidTicketWithInValidDiscountCard_Return403(){
        String ticket = this.ticketList.get(0);
        String validation = "{\n" +
                "    \"ticketId\": "+ticket+" ,\n" +
                "    \"zone\": \"A\" ,\n" +
                "    \"date\": \"2020-10-29T10:38:59\" ,\n" +
                "    \"discountCardId\": 666 ,\n" +
                "    \"disabled\": true ,\n" +
                "    \"student\": true\n" +
                "}";
        given().
                contentType(ContentType.JSON).
                body(validation)
                .post("/tickets/validate").then().statusCode(403);

    }


    @ParameterizedTest
    @ValueSource(strings = {
            "{\n" +
                    "    \"ticketId\": TICKETID ,\n" +
                    "    \"zone\": \"A\" ,\n" +
                    "    \"date\": \"2020-10-29T10:39:59\" ,\n" +
                    "    \"disabled\": false ,\n" +
                    "    \"student\": true\n" +
                    "}",
            "{\n" +
                    "    \"ticketId\": TICKETID ,\n" +
                    "    \"zone\": \"A\" ,\n" +
                    "    \"date\": \"2020-10-29T10:39:59\" ,\n" +
                    "    \"disabled\": true ,\n" +
                    "    \"student\": false\n" +
                    "}",
            "{\n" +
                    "    \"ticketId\": TICKETID ,\n" +
                    "    \"zone\": \"A\" ,\n" +
                    "    \"date\": \"2022-10-29T10:39:59\" ,\n" +
                    "    \"disabled\": true ,\n" +
                    "    \"student\": true\n" +
                    "}",
            "{\n" +
                    "    \"ticketId\": TICKETID ,\n" +
                    "    \"zone\": \"B\" ,\n" +
                    "    \"date\": \"2022-10-29T10:39:59\" ,\n" +
                    "    \"disabled\": true ,\n" +
                    "    \"student\": true\n" +
                    "}",
            "{\n" +
                    "    \"ticketId\": 42069 ,\n" +
                    "    \"zone\": \"B\" ,\n" +
                    "    \"date\": \"2022-10-29T10:39:59\" ,\n" +
                    "    \"disabled\": true ,\n" +
                    "    \"student\": true\n" +
                    "}"
    })
    public void validateTicket_giveInvalidTicket_Return403(String validation){
        String ticket = this.ticketList.get(0);
        validation=validation.replace("TICKETID",ticket);

        given().
                contentType(ContentType.JSON).
                body(validation)
                .post("/tickets/validate").then().statusCode(403);
    }

}
