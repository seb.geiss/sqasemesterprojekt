package de.rwth.swc.sqa;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

import io.restassured.matcher.RestAssuredMatchers;
import netscape.javascript.JSObject;
import org.hamcrest.Matchers.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import java.util.LinkedList;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
@TestInstance(Lifecycle.PER_CLASS)
public class PostDiscountCardTest {
    private static final String PATH = "/customers/1/discountcards";

    private List<String> customerList = new LinkedList<String>();

    @LocalServerPort
    private int port;
    @BeforeEach
    public void setUp() {
        RestAssured.port = port;
    }

    @BeforeAll
    @Test
    public void createCustomerForValidation()
    {
        setUp();
        String customer = "{\"birthdate\" : \"1992-01-01\"" +
                ",\"disabled\" : true," +
                " \"id\" : 11}";

        this.customerList.add(given().
                contentType(ContentType.JSON).
                body(customer)
                .post("/customers")
                .getBody()
                .jsonPath()
                .get("id").toString());

        customer = "{\"birthdate\" : \"1992-01-01\"" +
                ",\"disabled\" : true," +
                " \"id\" : 13}";

        this.customerList.add(given().
                contentType(ContentType.JSON).
                body(customer)
                .post("/customers")
                .getBody()
                .jsonPath()
                .get("id").toString());

    }



    @Test
    public void postDiscountCard_giveValidDiscountCard_returnDiscountCard(){
        String customer = customerList.get(0);
        String discountCard =  "{ \"validFor\" : \"30d\"," +
                " \"customerId\" : "+customer+", " +
                "\"id\" : 0, " +
                "\"validFrom\" : \"1992-01-01\", " +
                "\"type\" : 25 }";

        given().
                contentType(ContentType.JSON).
                body(discountCard)
                .post("/customers/"+customer+"/discountcards").then()
                .statusCode(201)
                .body(
                        "id", org.hamcrest.Matchers.is(org.hamcrest.Matchers.notNullValue()),
                    "customerId",  org.hamcrest.Matchers.is(Long.parseLong(customer)),
                            "type", org.hamcrest.Matchers.is(25),
                            "validFrom", org.hamcrest.Matchers.is("1992-01-01"),
                            "validFor", org.hamcrest.Matchers.is("30d")
                );

    }

    @Test
    public void postDiscountCard_whenDiscountCardAlreadyExisting_return409(){
        String customer = customerList.get(0);
        String discountCard =  "{ \"validFor\" : \"30d\"," +
                " \"customerId\" : "+customer+", " +
                "\"id\" : 0, " +
                "\"validFrom\" : \"1993-01-02\", " +
                "\"type\" : 25 }";

        given().
                contentType(ContentType.JSON).
                body(discountCard)
                .post("/customers/"+customer+"/discountcards");

        given().
                contentType(ContentType.JSON).
                body(discountCard)
                .post("/customers/"+customer+"/discountcards").then()
                .statusCode(409);

    }

    @Test
    public void postDiscountCard_whenCustomerNotExisting_return404(){
        String discountCard =  "{ \"validFor\" : \"30d\"," +
                " \"customerId\" : 42069, " +
                "\"id\" : 0, " +
                "\"validFrom\" : \"1992-01-02\", " +
                "\"type\" : 25 }";

        given().
                contentType(ContentType.JSON).
                body(discountCard)
                .post("/customers/42069/discountcards")
                .then()
                .statusCode(404);;
    }

    @Test
    public void postDiscountCard_whenCustomerIdNotEqualToInputId_return404(){
        String customer = customerList.get(1);
        String discountCard =  "{ \"validFor\" : \"30d\"," +
                " \"customerId\" : 42069, " +
                "\"id\" : 0, " +
                "\"validFrom\" : \"1992-01-02\", " +
                "\"type\" : 25 }";

        given().
                contentType(ContentType.JSON).
                body(discountCard)
                .post("/customers/"+customer+"/discountcards")
                .then()
                .statusCode(400);;
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "",
            "{ \"validFor\" : \" \"," +
                " \"customerId\" CUSTOMER: , " +
                "\"id\" : 0, " +
                "\"validFrom\" : \"1992-01-02\", " +
                "\"type\" : 25 }",
            "{ \"validFor\" : \"30d\"," +
                    " \"customerId\" : \"1992-01-02\", " +
                    "\"id\" : 0, " +
                    "\"validFrom\" : \"1992-01-02\", " +
                    "\"type\" : 25 }",
            "{ \"validFor\" : \"30d\"," +
                    " \"customerId\" : CUSTOMER, " +
                    "\"id\" : 0, " +
                    "\"validFrom\" : \"19920102\", " +
                    "\"type\" : 25 }",
            "{ \"validFor\" : \"30d\"," +
                    " \"customerId\" : CUSTOMER, " +
                    "\"id\" : 0, " +
                    "\"validFrom\" : \"1992-01-02\", " +
                    "\"type\" : 12 }",
            "{ \"validFor\" : \"125d\"," +
                    " \"customerId\" : CUSTOMER, " +
                    "\"id\" : 0, " +
                    "\"validFrom\" : \"1992-01-02\", " +
                    "\"type\" : 25 }"
    })
    public void postDiscountCard_whenInvalidInput_return400(String discountCard){

        discountCard=discountCard.replace("CUSTOMER",customerList.get(1));

        given().
                contentType(ContentType.JSON).
                body(discountCard)
                .post("/customers/13/discountcards")
                .then()
                .statusCode(400);;
    }





}