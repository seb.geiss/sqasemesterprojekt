package de.rwth.swc.sqa;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

import io.restassured.matcher.RestAssuredMatchers;
import org.hamcrest.Matchers.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import static io.restassured.RestAssured.given;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
public class CustomerTest {
    private static final String PATH = "/customers/1/discountcards";

    @LocalServerPort
    private int port;
    @BeforeEach
    public void setUp() {
        RestAssured.port = port;
    }


    //Test correct Reports:

    @Test
    public void customers_giveValidCustomer_ResponseOk()
    {
        String customer = "{\"birthdate\" : \"1992-01-01\",\"disabled\" : true, \"id\" : 1}";
        given().
                contentType(ContentType.JSON).
                body(customer)
                .post("/customers").
                then().statusCode(201);


    }

    @Test
    public void customers_giveInvalidUserWithoutProperties_Response400()
    {
        String customer = "{}";

        given().
                contentType(ContentType.JSON).
                body(customer)
                .post("/customers").
                then().statusCode(400);
    }

    @Test
    public void customers_giveInvalidUserWithWrongProperties_Response400()
    {
        String customer = "{\"date_of_birth\" : \"1992-01-01\",\"disabled_customer\" : true, \"myD\" : 1}";

        given().
                contentType(ContentType.JSON).
                body(customer)
                .post("/customers").
                then().statusCode(400);
    }

    @Test
    public void customers_giveInvalidFormat_Response415()
    {
        String customer = "{\"date_of_birth\" : \"1992-01-01\",\"disabled_customer\" : true, \"myD\" : 1}";

        given().
                contentType(ContentType.XML).
                body(customer)
                .post("/customers").
                then().statusCode(415);
    }




    //description: "Adds a new customer that can order cards. The id must not be set in the request but has to be present in the response.
    // The disabled flag should default to false if not set in the request.
    // Make sure that the status codes are reported correctly."






    //Test correct input:

    @Test
    public void customers_giveValidCustomer_ResponseCustomer()
    {
        String customer = "{\"birthdate\" : \"1992-01-01\",\"disabled\" : true, \"id\" : 1}";
        given().
                contentType(ContentType.JSON).
                body(customer)
                .post("/customers").
                then()
                .body(
                        "birthdate", org.hamcrest.Matchers.is("1992-01-01"),
                        "disabled", org.hamcrest.Matchers.is(true),
                        "id", org.hamcrest.Matchers.is(org.hamcrest.Matchers.notNullValue())
                );

    }


    @Test
    public void customers_giveValidCustomerWithoutId_ResponseSameCustomerWithID()
    {
        String customer = "{\"birthdate\" : \"1992-01-01\",\"disabled\" : true}";
        given().
                contentType(ContentType.JSON).
                body(customer)
                .post("/customers").
                then()
                .body(
                        "birthdate", org.hamcrest.Matchers.is("1992-01-01"),
                        "disabled", org.hamcrest.Matchers.is(true),
                        "id", org.hamcrest.Matchers.notNullValue()
                );

    }

    @Test
    public void customers_giveValidCustomerWithoutDisabledinformation_ResponseSameCustomerWithDisabledFalse()
    {
        String customer = "{\"birthdate\" : \"1992-01-01\",\"id\" : 2}";
        given().
                contentType(ContentType.JSON).
                body(customer)
                .post("/customers").
                then()
                .body(
                        "birthdate", org.hamcrest.Matchers.is("1992-01-01"),
                        "disabled", org.hamcrest.Matchers.is(false),
                        "id", org.hamcrest.Matchers.is(org.hamcrest.Matchers.notNullValue())
                );

    }


    //Test missunderstanding in API description -> id must not be defined/must not be used by service

    @Test
    public void customers_giveValidCustomersWithSameId_Response2CustomersWithDifferentId()
    {
        String customer;
        customer = "{\"birthdate\" : \"1992-01-01\",\"disabled\" : true, \"id\" : 1}";
        String id = given().
                contentType(ContentType.JSON).
                body(customer)
                .post("/customers").getBody().jsonPath().get("id").toString();

        customer = "{\"birthdate\" : \"1922-01-01\",\"disabled\" : false, \"id\" : 1}";

        given().
                contentType(ContentType.JSON).
                body(customer)
                .post("/customers").then().body(
                        "birthdate", org.hamcrest.Matchers.is("1922-01-01"),
                        "disabled", org.hamcrest.Matchers.is(false),
                        "id", org.hamcrest.Matchers.is(org.hamcrest.Matchers.not(Long.valueOf(id)))
                );



    }



}
