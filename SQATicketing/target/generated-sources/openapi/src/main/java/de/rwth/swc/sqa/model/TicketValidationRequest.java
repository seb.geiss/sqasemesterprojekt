package de.rwth.swc.sqa.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * TicketValidationRequest
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-05-13T12:46:49.120636600+02:00[Europe/Berlin]")
public class TicketValidationRequest   {
  @JsonProperty("ticketId")
  private Long ticketId;

  /**
   * Zone the validator is in
   */
  public enum ZoneEnum {
    A("A"),
    
    B("B"),
    
    C("C");

    private String value;

    ZoneEnum(String value) {
      this.value = value;
    }

    @JsonValue
    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static ZoneEnum fromValue(String value) {
      for (ZoneEnum b : ZoneEnum.values()) {
        if (b.value.equals(value)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }
  }

  @JsonProperty("zone")
  private ZoneEnum zone;

  @JsonProperty("date")
  private String date;

  @JsonProperty("disabled")
  private Boolean disabled;

  @JsonProperty("discountCardId")
  private Long discountCardId;

  @JsonProperty("student")
  private Boolean student;

  public TicketValidationRequest ticketId(Long ticketId) {
    this.ticketId = ticketId;
    return this;
  }

  /**
   * Get ticketId
   * @return ticketId
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public Long getTicketId() {
    return ticketId;
  }

  public void setTicketId(Long ticketId) {
    this.ticketId = ticketId;
  }

  public TicketValidationRequest zone(ZoneEnum zone) {
    this.zone = zone;
    return this;
  }

  /**
   * Zone the validator is in
   * @return zone
  */
  @ApiModelProperty(required = true, value = "Zone the validator is in")
  @NotNull


  public ZoneEnum getZone() {
    return zone;
  }

  public void setZone(ZoneEnum zone) {
    this.zone = zone;
  }

  public TicketValidationRequest date(String date) {
    this.date = date;
    return this;
  }

  /**
   * Get date
   * @return date
  */
  @ApiModelProperty(example = "2020-10-29T10:38:59", required = true, value = "")
  @NotNull


  public String getDate() {
    return date;
  }

  public void setDate(String date) {
    this.date = date;
  }

  public TicketValidationRequest disabled(Boolean disabled) {
    this.disabled = disabled;
    return this;
  }

  /**
   * Get disabled
   * @return disabled
  */
  @ApiModelProperty(value = "")


  public Boolean getDisabled() {
    return disabled;
  }

  public void setDisabled(Boolean disabled) {
    this.disabled = disabled;
  }

  public TicketValidationRequest discountCardId(Long discountCardId) {
    this.discountCardId = discountCardId;
    return this;
  }

  /**
   * Get discountCardId
   * @return discountCardId
  */
  @ApiModelProperty(value = "")


  public Long getDiscountCardId() {
    return discountCardId;
  }

  public void setDiscountCardId(Long discountCardId) {
    this.discountCardId = discountCardId;
  }

  public TicketValidationRequest student(Boolean student) {
    this.student = student;
    return this;
  }

  /**
   * Get student
   * @return student
  */
  @ApiModelProperty(value = "")


  public Boolean getStudent() {
    return student;
  }

  public void setStudent(Boolean student) {
    this.student = student;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TicketValidationRequest ticketValidationRequest = (TicketValidationRequest) o;
    return Objects.equals(this.ticketId, ticketValidationRequest.ticketId) &&
        Objects.equals(this.zone, ticketValidationRequest.zone) &&
        Objects.equals(this.date, ticketValidationRequest.date) &&
        Objects.equals(this.disabled, ticketValidationRequest.disabled) &&
        Objects.equals(this.discountCardId, ticketValidationRequest.discountCardId) &&
        Objects.equals(this.student, ticketValidationRequest.student);
  }

  @Override
  public int hashCode() {
    return Objects.hash(ticketId, zone, date, disabled, discountCardId, student);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TicketValidationRequest {\n");
    
    sb.append("    ticketId: ").append(toIndentedString(ticketId)).append("\n");
    sb.append("    zone: ").append(toIndentedString(zone)).append("\n");
    sb.append("    date: ").append(toIndentedString(date)).append("\n");
    sb.append("    disabled: ").append(toIndentedString(disabled)).append("\n");
    sb.append("    discountCardId: ").append(toIndentedString(discountCardId)).append("\n");
    sb.append("    student: ").append(toIndentedString(student)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

