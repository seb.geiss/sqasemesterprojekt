package de.rwth.swc.sqa.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.openapitools.jackson.nullable.JsonNullable;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * TicketRequest
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-05-13T12:46:49.120636600+02:00[Europe/Berlin]")
public class TicketRequest   {
  @JsonProperty("validFrom")
  private String validFrom;

  @JsonProperty("birthdate")
  private String birthdate;

  /**
   * Gets or Sets validFor
   */
  public enum ValidForEnum {
    _1H("1h"),
    
    _1D("1d"),
    
    _30D("30d"),
    
    _1Y("1y");

    private String value;

    ValidForEnum(String value) {
      this.value = value;
    }

    @JsonValue
    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static ValidForEnum fromValue(String value) {
      for (ValidForEnum b : ValidForEnum.values()) {
        if (b.value.equals(value)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }
  }

  @JsonProperty("validFor")
  private ValidForEnum validFor;

  @JsonProperty("disabled")
  private Boolean disabled;

  @JsonProperty("discountCard")
  private Boolean discountCard;

  /**
   * Gets or Sets zone
   */
  public enum ZoneEnum {
    A("A"),
    
    B("B"),
    
    C("C");

    private String value;

    ZoneEnum(String value) {
      this.value = value;
    }

    @JsonValue
    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static ZoneEnum fromValue(String value) {
      for (ZoneEnum b : ZoneEnum.values()) {
        if (b.value.equals(value)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }
  }

  @JsonProperty("zone")
  private ZoneEnum zone;

  @JsonProperty("student")
  private Boolean student;

  public TicketRequest validFrom(String validFrom) {
    this.validFrom = validFrom;
    return this;
  }

  /**
   * Get validFrom
   * @return validFrom
  */
  @ApiModelProperty(example = "2020-10-29T10:38:59", value = "")


  public String getValidFrom() {
    return validFrom;
  }

  public void setValidFrom(String validFrom) {
    this.validFrom = validFrom;
  }

  public TicketRequest birthdate(String birthdate) {
    this.birthdate = birthdate;
    return this;
  }

  /**
   * Get birthdate
   * @return birthdate
  */
  @ApiModelProperty(example = "1992-01-01", required = true, value = "")
  @NotNull


  public String getBirthdate() {
    return birthdate;
  }

  public void setBirthdate(String birthdate) {
    this.birthdate = birthdate;
  }

  public TicketRequest validFor(ValidForEnum validFor) {
    this.validFor = validFor;
    return this;
  }

  /**
   * Get validFor
   * @return validFor
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public ValidForEnum getValidFor() {
    return validFor;
  }

  public void setValidFor(ValidForEnum validFor) {
    this.validFor = validFor;
  }

  public TicketRequest disabled(Boolean disabled) {
    this.disabled = disabled;
    return this;
  }

  /**
   * Get disabled
   * @return disabled
  */
  @ApiModelProperty(value = "")


  public Boolean getDisabled() {
    return disabled;
  }

  public void setDisabled(Boolean disabled) {
    this.disabled = disabled;
  }

  public TicketRequest discountCard(Boolean discountCard) {
    this.discountCard = discountCard;
    return this;
  }

  /**
   * Get discountCard
   * @return discountCard
  */
  @ApiModelProperty(value = "")


  public Boolean getDiscountCard() {
    return discountCard;
  }

  public void setDiscountCard(Boolean discountCard) {
    this.discountCard = discountCard;
  }

  public TicketRequest zone(ZoneEnum zone) {
    this.zone = zone;
    return this;
  }

  /**
   * Get zone
   * @return zone
  */
  @ApiModelProperty(value = "")


  public ZoneEnum getZone() {
    return zone;
  }

  public void setZone(ZoneEnum zone) {
    this.zone = zone;
  }

  public TicketRequest student(Boolean student) {
    this.student = student;
    return this;
  }

  /**
   * Get student
   * @return student
  */
  @ApiModelProperty(value = "")


  public Boolean getStudent() {
    return student;
  }

  public void setStudent(Boolean student) {
    this.student = student;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TicketRequest ticketRequest = (TicketRequest) o;
    return Objects.equals(this.validFrom, ticketRequest.validFrom) &&
        Objects.equals(this.birthdate, ticketRequest.birthdate) &&
        Objects.equals(this.validFor, ticketRequest.validFor) &&
        Objects.equals(this.disabled, ticketRequest.disabled) &&
        Objects.equals(this.discountCard, ticketRequest.discountCard) &&
        Objects.equals(this.zone, ticketRequest.zone) &&
        Objects.equals(this.student, ticketRequest.student);
  }

  @Override
  public int hashCode() {
    return Objects.hash(validFrom, birthdate, validFor, disabled, discountCard, zone, student);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TicketRequest {\n");
    
    sb.append("    validFrom: ").append(toIndentedString(validFrom)).append("\n");
    sb.append("    birthdate: ").append(toIndentedString(birthdate)).append("\n");
    sb.append("    validFor: ").append(toIndentedString(validFor)).append("\n");
    sb.append("    disabled: ").append(toIndentedString(disabled)).append("\n");
    sb.append("    discountCard: ").append(toIndentedString(discountCard)).append("\n");
    sb.append("    zone: ").append(toIndentedString(zone)).append("\n");
    sb.append("    student: ").append(toIndentedString(student)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

